import React from "react";
import "../assets/main.css";

//i will create a jsx code tom
const Todo = ({ text, todo, todos, setTodos }) => {
	const deleteHandler = () => {
		 setTodos(todos.filter((el) => el.id !== todo.id));
	};
	const completeHadler = () => {
		setTodos(todos.map((item) => {
			if(item.id === todo.id){
				return {
					...item, completed: !item.completed
				};
			}
			return item;
		})
		);
	};
	return(
		<div className=" flex justify-center space-x-5">
		<div className="bg-white text-center m-3 rounded-xl p- w-5/6">
			<div className="todo">
		
			<li className={`todo-item ${todo.completed 
				?
				 "completed" 
				:
				 ""}`}>{text}
			</li>
			<button onClick={completeHadler} className="complete-btn">
			<i className="fas fa-check" ></i>
			</button>
	

			<button onClick={deleteHandler} className="trash-btn">
			<i className="fas fa-trash"></i>
			</button>
			</div>
			</div>
			</div>

		);
};
 export default Todo;