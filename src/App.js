import React, { useState, useEffect } from "react";
import "./assets/main.css";
import Form from "./components/Form";
import Todolist from "./components/Todolist"

function App() {
   const [inputText, setInputText] = useState("");
  const [todos, setTodos] = useState([]);
  const [status,setStatus] = useState('all');
  const [filteredTodos, setFilteredTodos] = useState([]);

  useEffect(() => {
  getLocalTodos()
    getLocalTodos();
  }, []);

   useEffect(() => {
   filterHandler();
   saveLocalTodos();
  },[todos, status]);

  const filterHandler = () => {
    switch(status){
      case "completed": 
      setFilteredTodos(todos.filter(todo => todo.completed === true ))
      break;
      case "uncompleted":
      setFilteredTodos(todos.filter(todo => todo.completed === false ))
         break;
      default:
      setFilteredTodos(todos);
      break;
    }
  };
const saveLocalTodos = () => {
   if(localStorage.getItem("todos") === null) {localStorage.setItem("todos", JSON.stringify([]));
  }else{
    localStorage.setItem("todos", JSON.stringify(todos));
  }
};
const getLocalTodos = () => {
  if(localStorage.getItem("todos") === null) {localStorage.setItem("todos", JSON.stringify([]));
  }else{
    let todoLocal = JSON.parse(localStorage.getItem("todos"));
    setTodos(todoLocal);
  }
};


  return (
     <div className="App" >
        <div className=" justify-items-center">
            <h2 className="bg-pink-800 text-white text-center mt-20 max-w-2xl font-mono shadow-xl text-5xl font-medium m-auto mb-5 p-7 border-pink-500 rounded-lg">My Todo List:</h2>
        </div>
 
      <Form inputText={inputText} todos={todos} setTodos={setTodos} setInputText={setInputText} 
        setStatus={setStatus} 
        />
      <Todolist 
      filteredTodos={filteredTodos} 
      setTodos={setTodos} 
      todos={todos} 
      />
    </div> 
   
  );
}

export default App;
